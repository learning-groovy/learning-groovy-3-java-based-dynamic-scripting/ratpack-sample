package example.ratpack

class Config {
    DatabaseConfig database
    ClusterConfig cluster
}

class DatabaseConfig {
    String username = 'root'
    String password = ''
    String hostname = 'localhost'
    String database = 'myDb'
}

class ClusterConfig {
    String hostname = 'localhost'
    String port = '5050'
}
