package example.ratpack

import groovy.transform.Canonical

@Canonical
class Person {
    String name
    int age
}
