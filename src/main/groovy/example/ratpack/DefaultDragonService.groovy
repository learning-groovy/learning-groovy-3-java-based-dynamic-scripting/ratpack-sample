package example.ratpack

class DefaultDragonService implements DragonService {
    @Override
    List<Dragon> list() {
        return [new Dragon('Smaug', 499), new Dragon('Norbert', 488)]
    }
}
