package example.ratpack

import groovy.transform.Canonical

@Canonical
class Dragon {
    String name
    int scale
}
