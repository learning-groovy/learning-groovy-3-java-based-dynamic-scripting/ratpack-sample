package ratpack

import ratpack.groovy.test.GroovyRatpackMainApplicationUnderTest
import spock.lang.Specification

class FunctionalSpec extends Specification {
    void 'default handler should return "Hello from Ratpack"'() {
        setup:
        def app = new GroovyRatpackMainApplicationUnderTest()

        when:
        def response = app.httpClient.text

        then:
        response == 'Hello from Ratpack'

        cleanup:
        app.close()
    }

    void 'should properly render "get foo"'() {
        setup:
        def app = new GroovyRatpackMainApplicationUnderTest()

        when:
        def response = app.httpClient.requestSpec { spec ->
            spec.headers.'User-Agent' = ['Client v2.0']
        }.get('foo').body.text

        then:
        response == 'get foo'

        cleanup:
        app.close()
    }
}
