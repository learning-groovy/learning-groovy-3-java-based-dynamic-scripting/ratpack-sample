package example.ratpack

import spock.lang.Specification

class DragonServiceSpec extends Specification {
    void 'default service should return a list of dragons'() {
        setup:
        'Set up the service for testing'
        def service = new DefaultDragonService()

        when:
        'Perform the service call'
        def result = service.list()

        then:
        'Ensure that the service call returned the proper result'
        result.size() == 2
    }
}
