import example.ratpack.ClusterConfig
import example.ratpack.Database
import example.ratpack.DatabaseConfig
import example.ratpack.DefaultDragonService
import example.ratpack.DragonService
import example.ratpack.Person
import ratpack.exec.Blocking
import ratpack.groovy.template.MarkupTemplateModule
import ratpack.groovy.template.TextTemplateModule
import ratpack.handlebars.HandlebarsModule
import ratpack.thymeleaf3.ThymeleafModule

import static ratpack.core.jackson.Jackson.fromJson
import static ratpack.core.jackson.Jackson.json
import static ratpack.groovy.Groovy.groovyMarkupTemplate
import static ratpack.groovy.Groovy.groovyTemplate
import static ratpack.groovy.Groovy.ratpack
import static ratpack.handlebars.Template.handlebarsTemplate
import static ratpack.thymeleaf3.Template.thymeleafTemplate

ratpack {
    def database = new Database()
    serverConfig {
        json 'config.json'
        yaml 'config.yml'
        sysProps()
        env()
        require('/database', DatabaseConfig)
        require('/cluster', ClusterConfig)
    }
    bindings {
        module MarkupTemplateModule
        module TextTemplateModule
        module HandlebarsModule
        module ThymeleafModule
        bind(DragonService, DefaultDragonService)
    }
    handlers {
        all() { response.headers.add('x-custom', 'x'); next() }
        get('db-config') { DatabaseConfig config ->
            render json(config)
        }
        get('cluster-config') { ClusterConfig config ->
            render json(config)
        }
        get {
            render 'Hello from Ratpack'
        }
        get('deleteOlderThan/:days') {
            int days = pathTokens.days as int
            Blocking.get { database.deleteOlderThan(days) }
                .then { int i -> render("$i records deleted") }
        }
        get('dragons') { DragonService dService ->
            dService.list().with { dragons ->
                render(json(dragons))
            }
        }
        path('foo') {
            byMethod {
                get() { render 'get foo' }
                post() { render 'post foo' }
                put() { render 'put foo' }
                delete() { render 'delete foo' }
            }
        }
        get('bar') {
            render handlebarsTemplate('myTemplate.html', title: 'Handlebars')
        }
        get('leaf') {
            render thymeleafTemplate('myTemplate', [title: 'Thymeleaf'])
        }
        get('user') {
            render json([user: 1])
        }
        get('gtpl/:key') {
            def key = pathTokens.key
            render groovyMarkupTemplate('index.gtpl', title: "$key")
        }
        get('text/:key') {
            def key = pathTokens.key
            render groovyTemplate('index.html', title: "$key")
        }
        post('personNames') {
            render(parse(fromJson(Person.class)).map { it.name })
        }
        files { dir 'public' }
    }
}
